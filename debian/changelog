cclib-data (1.6.2-4) UNRELEASED; urgency=medium

  [ Michael Banck ]
  * debian/watch: Updated.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Sun, 27 Nov 2022 11:02:28 +0100

cclib-data (1.6.2-2) unstable; urgency=medium

  * Team Upload.
  * Update imports for new version of openbabel (Closes: #956149).
  * Update Standards-Version to 4.5.0 (no changes required).
  * Move to debhelper-compat (= 12).

 -- Stuart Prescott <stuart@debian.org>  Sun, 12 Apr 2020 18:32:35 +1000

cclib-data (1.6.2-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
    - License has been changed in version 1.5 from LGPL to 3-Clause BSD
      (see debian/copyright).

  [ Stuart Prescott ]
  * New upstream release.
  * Drop Python 2 from build system; use Python 3 instead (Closes: #936276).
  * Update test suite patches.
  * Update Standards-Version to 4.4.1 (no changes required).

  [ Daniel Leidert ]
  * debian/compat: Raised to debhelper compat level 11.
  * debian/control: Dropped obsolete X-Python-Version.
    (Vcs*): Adjusted after move to salsa.d.o.
    (Build-Depends): Raised to dh version 11.
    (Uploaders): Removed Karol Langner. Thanks for your work.
    (Standards-Version): Bumped to 4.1.4.
  * debian/copyright: Updated and rewritten in DEP5 format. Upstream changed
    license from LGPL to BSD.
  * debian/upstream: Renamed to debian/upstream/metadata.
  * debian/watch: Fixed after project moved to github.

  [ Michael Banck ]
  * debian/patches/fix_setup.py: Updated.
  * debian/patches/fix_testsuite.patch: New patch, allows the test suite to be
    run from any directory.
  * debian/control (Build-Depends): Added python-cclib and python-openbabel.
  * debian/rules (override_dh_auto_test): New target, runs the tests.

 -- Stuart Prescott <stuart@debian.org>  Sat, 11 Jan 2020 02:15:11 +1100

cclib-data (1.1-1) unstable; urgency=low

  * New upstream release.

  [ Daniel Leidert ]
  * debian/control: Added Provides and Enhances. Removed duplicated section.
    (Build-Depends): Added missing python depends and python-numpy.
    (Standards-Version): Bumped to 3.9.4.
  * debian/rules: Added dh_numpy call.
  * debian/upstream: Added.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Mon, 01 Apr 2013 15:44:06 +0200

cclib-data (1.0.1-3) unstable; urgency=low

  * debian/control: Remove +ssh from Vcs-Svn.

 -- Karol M. Langner <karol.langner@gmail.com>  Thu, 19 Apr 2012 12:43:24 +0200

cclib-data (1.0.1-2) unstable; urgency=low

  * Renamed the python module and use dh_python2 (closes: Bug#640935).

 -- Karol M. Langner <karol.langner@gmail.com>  Fri, 09 Sep 2011 13:30:01 +0100

cclib-data (1.0.1-1) unstable; urgency=low

  * Initial release.

 -- Karol M. Langner <karol.langner@gmail.com>  Sun, 26 Jul 2011 15:28:14 +0100
